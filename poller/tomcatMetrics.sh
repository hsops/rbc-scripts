#!/bin/bash

# Check to see if the process is already being run
PollingCount=$(ps ax | grep -i "PollingAgent.jar" | grep -v "grep" | grep -ic "PollingAgent.jar")

JAVA_JDK=$(find /usr/lib/jvm/ -name java | grep "jdk" | head -n 1)

if [ "${PollingCount}" -eq 0 ]; then

  pushd "/opt/rbc/poller" || exit 0

  ./context-updates.sh

  ${JAVA_JDK} -jar PollingAgent.jar

  popd || exit 0

fi
