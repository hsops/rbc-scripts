#!/bin/bash

pushd "/opt/rbc/poller/" || exit 0

CONTEXT=$(find /usr/local/tomcat/webapps/ -name "*.war" -type f | head -n 1 | xargs basename | awk -F'.' '{ print $1 }')

echo "Context = ${CONTEXT}"

LOG_CONTEXT=$(cat http.xml | grep -Eo '/[a-z]+/' | head -n 1 | awk -F'/' '{ print $2 }')

echo "Logged Context = ${LOG_CONTEXT}"

HIT_COUNT=$(cat http.xml | grep -Eo '/[a-z]+/' | grep -c "/${CONTEXT}/")

if [[ ! "${CONTEXT}" == "${LOG_CONTEXT}" ]]; then

  echo "Need to update files - Hits = ${HIT_COUNT}"

  RESULT=$(sed -i "s/${LOG_CONTEXT}/${CONTEXT}/g" http.xml)
  echo "Result updating http.xml - ${RESULT}"

  RESULT=$(sed -i "s/localhost\/${LOG_CONTEXT}/localhost\/${CONTEXT}/g" jmx.xml)
  echo "Result updating jmx.xml - ${RESULT}"

else

  echo "All files configured properly - Hits = ${HIT_COUNT}"

fi

popd || exit 0