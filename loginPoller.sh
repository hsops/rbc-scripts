# =========================================================
# Perform Login Tests to see how this server is acting
# 
# Created 2/5/2017
# By Matt W
# =========================================================

TEST_USERNAME="maldaer"
TEST_PASSWORD="topgun"

TEST_COMMAND="curl -s http://127.0.0.1/hs/login.hs?username=${TEST_USERNAME}&password=${TEST_PASSWORD}"

# I do not want all the servers that are synced to a clock hitting 
# the system all at the same time, could skew results
RND_WAIT=$(( ( RANDOM % 60 ) + 1 ))
sleep ${RND_WAIT}

# Date stamp for start
LOGIN_START_TIME=$(date +%s%N | cut -b1-13)

# Perform a login
TEST_RESULT=$(${TEST_COMMAND} | grep "id=\"eid\"" | grep -Eo "[0-9]+")

# Date stamp for completion
LOGIN_END_TIME=$(date +%s%N | cut -b1-13)

# How long did it take to perform a login (seconds)
LOGIN_TIME=$((${LOGIN_END_TIME}-${LOGIN_START_TIME}))

# Status of login, was it successful and how long did that take
TEST_STATUS="Success"
if [[ -z ${TEST_RESULT} ]]; then
        TEST_STATUS="Failed"
fi

# Output to logfile
LOG_DATE=$(date +'%Y-%m-%d %T')
LOG_FOLDER="/var/log/metrics"

if [[ ! -d "${LOG_FOLDER}" ]]; then
  mkdir -p ${LOG_FOLDER}
fi

LOG_FILE="${LOG_FOLDER}/login.log"

# Output results
echo "${LOG_DATE} ${TEST_STATUS} ${LOGIN_TIME}" >> ${LOG_FILE}

