#!/bin/bash

# Poll the file
# ok|not functioning correctly
# <br />
#
# unknown
# -- or --
# queue good|queue backed up
# <br />
# past due files/all waiting files
# <br />
# x/y
LOG_PATH="/var/log/metrics/fp-health-status.log"
HEALTH_URL="http://127.0.0.1/hsfp/health.jsp"

DO_POLL=$(find "/usr/local/tomcat/webapps/" -name "hsfp.war")

if [[ -z ${DO_POLL} ]]; then

  exit 0

fi

CURRENT_IFS=${IFS}

IFS='
'

Status="ok"
QueueStatus="ok"
PassedDueFiles=0
QueueLength=0
LineNo=0
for record in $(curl -s ${HEALTH_URL}); do
  LineNo=$((${LineNo}+1))
  # echo "Line (${LineNo}): ${record}"
  if [[ -z ${record} ]] && [[ ${LineNo} -lt 9 ]]; then
    echo "... blank record"
  elif [[ ${LineNo} -eq 9 ]]; then
    if [[ "${record}" == "ok" ]]; then
      echo "file processor is functioning properly"
    else
      echo "file processor is ${record}"
      Status="bad_state"
    fi
  elif [[ ${LineNo} -eq 11 ]]; then
    if [[ "${record}" == "unknown" ]]; then
      echo "unable to get a read on the queue"
      QueueStatus="unknown"
    elif [[ "${record}" == "queue good" ]]; then
      echo "Queue is operating normally"
    else
      echo "Queue is backed up"
      QueueStatus="backed_up"
    fi
  elif [[ ${LineNo} -eq 15 ]]; then
    PassedDueFiles=$(echo "${record}" | awk -F' / ' '{ print $1 }')
    QueueLength=$(echo "${record}" | awk -F' / ' '{ print $2 }')
  fi
done;

IFS=${CURRENT_IFS}

if [[ ${LineNo} -lt 10 ]]; then
  echo "Server offline"
  Status="offline"
  QueueStatus="offline"
  PassedDueFiles=0
  QueueLength=0
fi

echo "$(date +'%Y-%m-%d %T') ${Status} ${QueueStatus} ${PassedDueFiles} ${QueueLength}" >> ${LOG_PATH}