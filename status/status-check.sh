#!/bin/bash

OUT_FILE="/var/log/metrics/status-check.log"

function writeLog() {
  echo "$(date +'%Y-%m-%d %T'),${1},${2}" >> ${OUT_FILE}
}

CHK1_WAR_LIST=$(find /usr/local -name *.war -exec basename {} \;)

if [[ -z ${CHK1_WAR_LIST} ]]; then

  writeLog "NO_WAR" "OFFLINE"
  exit 0

fi

for CHK1_WAR in $(find /usr/local -name *.war -exec basename {} \;); do

  CHK1_CONTEXT=$(echo "${CHK1_WAR}" | awk -F'.' '{ print $1 }')

  CHK2_HTTP=$(curl -s -I -m 5 http://127.0.0.1:8080/${CHK1_CONTEXT}/version.html | grep HTTP)

  if [[ -z ${CHK2_HTTP} ]]; then

    writeLog "${CHK1_CONTEXT}" "OFFLINE"

  else

    writeLog "${CHK1_CONTEXT}" "${CHK2_HTTP}"

  fi

done

exit 0
