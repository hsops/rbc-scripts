#!/bin/bash

TOMCAT_ROOT="/usr/local/tomcat"
TOMCAT_BIN="${TOMCAT_ROOT}/bin"

TARGET_FOLDER="/var/log/heapdumps"

JVM_CRASH_FILES=$(ls ${TOMCAT_BIN} | grep -E "[a-z]{1,}_err_pid[0-9]{1,}\.log" )

for CRASH_FILE in ${JVM_CRASH_FILES[@]}; do
  FILE_DATE=$(date -r ${TOMCAT_BIN}/${CRASH_FILE} +%F_%T | sed 's/:/-/g' | sed 's/-//g')
  echo "Renaming crash file to: ${HOSTNAME}_${FILE_DATE}_${CRASH_FILE}"
  mv ${TOMCAT_BIN}/${CRASH_FILE} ${TARGET_FOLDER}/${HOSTNAME}_${FILE_DATE}_${CRASH_FILE}
done

echo "Completed JVM Crash Archive"
