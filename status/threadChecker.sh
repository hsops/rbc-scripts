#!/bin/bash

# This script determines if the application is struggling based upon JVM thread counts.
# The peak JVM thread count is 150.  If it reaches this value, more than likely the application
# server will deteriorate
#
# We will poll the file poller.log for the last value it received.  If the time received or the status is offline
# we will have rundeck restart it, and then log the event.
# Poller.log gets populated every minute.  During deployments we will take the server offline for less than a minute but
# will be safe and give it 5 minutes to come back online so it doesn't interfere with deployments

METRICS_FOLDER="/var/log/metrics"

# File to determine status
CHK_FILE="${METRICS_FOLDER}/poller.log"
if [[ ! -f ${CHK_FILE} ]]; then
	echo "Polling file does not exist.  EXIT"
	exit 1
fi

TOMCAT_ROOT="/usr/local/tomcat"
TOMCAT_BIN="${TOMCAT_ROOT}/bin"
WORKING_FOLDER="/opt/rbc/status"

RUN_FILE="${WORKING_FOLDER}/lastRestart"
OFFLINE_FILE="${WORKING_FOLDER}/offlineCount"

updateOfflineStatus () {
  if [[ -z ${1} ]]; then
    echo "Updating offline count [resetting to 0]"
    echo "0" > ${OFFLINE_FILE}
  else
    echo "Updating offline count [setting to ${1}]"
    echo "${1}" > ${OFFLINE_FILE}
  fi
}

# Initialize offline file status
if [[ ! -f ${OFFLINE_FILE} ]]; then
  echo "Initializing count file"
  updateOfflineStatus
fi

# Check to see if we are restarting or deploying
F5_STATUS_FILE="${WORKING_FOLDER}/f5-flag"
if [[ -f ${F5_STATUS_FILE} ]]; then
  F5_STATUS=$(cat ${F5_STATUS_FILE})
  if [[ "${F5_STATUS}" == "disabled" ]]; then
    echo "$(date)" > ${RUN_FILE}
    exit 0
  fi
fi

# Offline period
SECONDS_OFFLINE="300"

# Maximum threads
MAX_THREADS="150"

# Max offline hits
MAX_OFFLINE=2

# CPU Metrics
CPU_POLLER_FILE="${METRICS_FOLDER}/cpu-stats.log"
CPU_AVERAGE=0
CPU_THRESHOLD=97
if [[ -f "${CPU_POLLER_FILE}" ]]; then
  CPU_AVERAGE=$(tail -5 ${CPU_POLLER_FILE} | grep -oE "usr=[0-9]{1,}\.[0-9]{2}" | awk -F'=' 'BEGIN{v=0.0}{ v = v + $2 }END{ v = v / 5.0; print v }')
  CPU_AVERAGE=${CPU_AVERAGE%.*}
fi

# What is my local IP
LOCAL_IP=$(ifconfig | grep "inet\saddr\:10\.1\." | awk -F' ' '{ print $2 }' | awk -F':' '{ print $2 }')

# Rundeck variables
RUNDECK_TOKEN="DtHCsRV59KsTVYMLxTJBPCiKwqOKiZgZ"
RUNDECK_SERVER="10.1.17.14"
RUNDECK_PORT="4440"
RUNDECK_JOB_UUID="d0ec7c3a-5a2d-4826-994e-196306588166"
RUNDECK_LOG="${METRICS_FOLDER}/rundeck.log"
RUNDECK_CALL="http://${RUNDECK_SERVER}:${RUNDECK_PORT}/api/1/job/${RUNDECK_JOB_UUID}/run?argString=-ServiceIPAddress%20${LOCAL_IP}"

# When did we get last record
LAST_RECORD=$(tail -n1 ${CHK_FILE} | awk -F' ' '{ print $1 " " $2 }')

# When did we last perform a restart
LAST_RESTART=$(date -d "Thu Jan 1 00:00:00 CST 1970" +"%s")
if [[ -f ${RUN_FILE} ]]; then
  LAST_RESTART=$(date -d "`cat ${RUN_FILE}`" +"%s")
fi

CURRENT_DATE=$(date +"%s")
LAST_POLL_DATE=$(date -d "${LAST_RECORD}" +"%s")
POLL_DIFF=$((${CURRENT_DATE}-${LAST_POLL_DATE}))
RESTART_DIFF=$((${CURRENT_DATE}-${LAST_RESTART}))

# Service Online/Offline
OFFLINE_COUNT=$(cat ${OFFLINE_FILE})
echo "Offline count = ${OFFLINE_COUNT}"
IS_HOST_ONLINE=$(tail -n1 ${CHK_FILE} | grep "Host is Offline")
HOST_STATUS="[Online]"
if [[ ! -z ${IS_HOST_ONLINE} ]]; then
  HOST_STATUS="[Offline]"
  OFFLINE_COUNT=$((${OFFLINE_COUNT}+1))
  echo "Offline count incremented to ${OFFLINE_COUNT}"
fi

# Safe limit < 150
THREAD_CHK=$(tail -n1 ${CHK_FILE} | grep -Eo 'currentThreadsBusy\=[0-9]+')
CURRENT_BUSY_THREADS="0"
if [[ ! -z ${THREAD_CHK} ]]; then
  CURRENT_BUSY_THREADS=$(echo ${THREAD_CHK} | awk -F'=' '{ print $2 }')
fi

if [[ ${OFFLINE_COUNT} -ge ${MAX_OFFLINE} ]]; then
  echo "Count will trigger the if statement"
fi

# JVM Crashes
JVM_CRASH_FILES=$(ls ${TOMCAT_BIN} | grep -E "[a-z]{1,}_err_pid[0-9]{1,}\.log" )
JVM_CRASH="false"
if [[ ! -z ${JVM_CRASH_FILES} ]]; then
  JVM_CRASH="true"
  echo "JVM Crashed"
  if [[ -f ${WORKING_FOLDER}/archive-jvm-crash-files.sh ]]; then
    ${WORKING_FOLDER}/archive-jvm-crash-files.sh
  fi
fi
echo "JVM Crash: ${JVM_CRASH}"

if [[ ${POLL_DIFF} -gt ${SECONDS_OFFLINE} || ${JVM_CRASH} == "true" || ${OFFLINE_COUNT} -ge ${MAX_OFFLINE} || ${CURRENT_BUSY_THREADS} -ge ${MAX_THREADS} || ${CPU_AVERAGE} -ge ${CPU_THRESHOLD} ]]; then

  RESTART_REASON="None Provided"
  if [[ ${POLL_DIFF} -gt ${SECONDS_OFFLINE} ]]; then
    echo "$(date) ${POLL_DIFF}seconds elapsed since last poll" >> ${RUNDECK_LOG}
    RESTART_REASON=$(echo "${POLL_DIFF}%20seconds%20elapsed")
  elif [[ ${CPU_AVERAGE} -ge ${CPU_THRESHOLD} ]]; then
    echo "$(date) CPU average (${CPU_AVERAGE}%) exceeds threshold of ${CPU_THRESHOLD}%" >> ${RUNDECK_LOG}
    RESTART_REASON=$(echo "CPU%20average%20(${CPU_AVERAGE}%25)%20exceeds%20threshold%20of%20${CPU_THRESHOLD}%25")
  elif [[ ${JVM_CRASH} == "true" ]]; then
    echo "$(date) JVM Crashed" >> ${RUNDECK_LOG}
    RESTART_REASON=$(echo "JVM%20Crashed")
  elif [[ ${OFFLINE_COUNT} -ge ${MAX_OFFLINE} ]]; then
    echo "$(date) Host is [OFFLINE-${OFFLINE_COUNT}]" >> ${RUNDECK_LOG}
    RESTART_REASON=$(echo "Application%20Offline")
  elif [[ ${CURRENT_BUSY_THREADS} -ge ${MAX_THREADS} ]]; then
    echo "$(date) Host has peaked maximum busy threads [${CURRENT_BUSY_THREADS}]"
    RESTART_REASON=$(echo "${CURRENT_BUSY_THREADS}%20threads")
  fi

  RUNDECK_CALL=$(echo "${RUNDECK_CALL}%20-RestartReason%20'${RESTART_REASON}'")

  # Restart only if we haven't tried restarting recently
  if [[ ${RESTART_DIFF} -gt ${SECONDS_OFFLINE} ]]; then
    # Execute the rundeck call
    RUNDECK_HTTP_RESPONSE=$(curl -H "X-RunDeck-Auth-Token: ${RUNDECK_TOKEN}" ${RUNDECK_CALL})

    echo "Called for a restart, setting count to 0"
    updateOfflineStatus

    IS_RUNDECK_ERROR=$(echo "${RUNDECK_HTTP_RESPONSE}" | grep -Eo "error='true'")
    if [[ -z ${IS_RUNDECK_ERROR} ]]; then
      # Make sure we don't spawn continuously
      echo "$(date)" > ${RUN_FILE}

      RUNDECK_HTTP_RESPONSE=$(echo ${RUNDECK_HTTP_RESPONSE} | sed 's/[0-9]\{4\}-[0-9]\{1,2\}-[0-9]\{1,2\}T[0-9]\{1,2\}:[0-9]\{1,2\}:[0-9]\{1,2\}Z/current date/')

      echo "$(date) ${RUNDECK_HTTP_RESPONSE}" >> ${RUNDECK_LOG}
    fi
  fi
elif [[ ${HOST_STATUS} == "[Offline]" ]]; then
  # Increment offline status
  echo "Updating with new value"
  updateOfflineStatus ${OFFLINE_COUNT}
else
  # Reset offline count if all is clean
  echo "All is well, setting to zero"
  updateOfflineStatus
fi
