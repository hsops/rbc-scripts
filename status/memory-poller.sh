#!/bin/bash

# Cron Job
#
# PATH=/usr/lib/sysstat:/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin
# 
# */1 * * * * root /opt/rbc/status/memory-poller.sh

LOG_FOLDER="/var/log/metrics"

if [[ ! -d "${LOG_FOLDER}" ]]; then
  mkdir -p ${LOG_FOLDER}
fi

SWAP_LOG="${LOG_FOLDER}/swap.log"
MEMORY_LOG="${LOG_FOLDER}/memory.log"
VMSTAT_LOG="${LOG_FOLDER}/vmstat.log"
FILE_QUEUE_LOG="${LOG_FOLDER}/filequeue.log"

# Memory
free -m  | grep "Mem" | awk -v date="$(date +'%Y-%m-%d %T')" -F' ' '{ print date ", type=Mem, total=" $2 ", used=" $3 ", free=" $4 }' >> ${MEMORY_LOG}

# Swap space
free -m  | grep "Swap" | awk -v date="$(date +'%Y-%m-%d %T')" -F' ' '{ print date ", type=Swap, total=" $2 ", used=" $3 ", free=" $4 }' >> ${SWAP_LOG}

# VMstats
vmstat -a | grep -v "free" | grep -v "swap" | awk -F' ' -v date="$(date +'%Y-%m-%d %T')" '{ print date ", type=VMStat, swpd=" $3 ", free=" $4 ", inactive=" $5 ", active=" $6 ", swapin=" $7 ", swapout=" $8 ", blocksrec=" $9 ", blockssent=" $10 ", interrupts=" $11 ", contextswitches=" $12 ", usercpu=" $13 ", systemcpu=" $14 ", idlecpu=" $15 ", cpuiowait=" $16 }' >> ${VMSTAT_LOG}

# File Record Queue - if File Processor
NAME_TEST=$(echo ${HOSTNAME} | grep -oE "DF-FileProcessor")
if [[ ! -v ${NAME_TEST} ]]; then
  curl -s -X GET http://127.0.0.1/hsfp/health.jsp | grep -oE "[0-9]{1,}\s+\/\s+[0-9]{1,}" | awk -F' ' -v date="$(date +'%Y-%m-%d %T')" '{print date ",old=" $1 ",queue=" $3 }' >> ${FILE_QUEUE_LOG}
fi
