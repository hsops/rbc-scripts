#!/bin/bash

# Cron Job
#
# PATH=/usr/lib/sysstat:/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin
# 
# */1 * * * * root /opt/rbc/status/garbageCollectionStats.sh

# HSCHG-905 Check to see if this script is currently running, exit if it is
ProcessName=$(basename ${0})
ProcessCheck=$(ps ax | grep ${ProcessName} | grep -v grep | head -n 1 | wc -l)
if [[ ! "${ProcessCheck}" == "1" ]]; then
  echo "Process [${ProcessName}] already running"
  exit 0
fi

LOG_FOLDER="/var/log/metrics"

if [[ ! -d "${LOG_FOLDER}" ]]; then
  mkdir -p ${LOG_FOLDER}
fi

GC_LOG="${LOG_FOLDER}/gc.log"

JSTAT=$(find /usr/lib/jvm/ -name jstat | head -n 1)

JSTAT_PROCESSES=$(ps ax | grep jstat | grep -v grep | awk -F' ' '{ print $1 }')
# Check for running cron jobs
if [[ ! -z ${JSTAT_PROCESSES} ]]; then
  for pids in ${JSTAT_PROCESSES[@]}; do
    kill ${pids}
  done
fi

# Garbage Collection
TC_PID=$(ps x | grep "java" | grep "tomcat" | grep "catalina" | grep -v "grep" | awk -F' ' '{ print $1 }')
${JSTAT} -gcutil ${TC_PID} | grep -v FGCT | awk -F' ' -v date="$(date +'%Y-%m-%d %T')" '{ print date ", YGC=" $6 ", YGCT=" $7 ", FGC=" $8 ", FGCT=" $9 }' >> ${GC_LOG}


