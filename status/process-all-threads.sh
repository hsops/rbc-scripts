#!/bin/bash

# Disabling

exit 0

outfolder="/var/log/tomcat/threads"

if [[ ! -d ${outfolder} ]]; then
  mkdir -p ${outfolder}
fi

SCRIPT_NAME=$(basename ${0})

infile="/tmp/thread-file-log.$(date +%Y-%m-%d-%H%M%S).tmp"
output="false"

# I do not want all the servers that are synced to a clock hitting 
# the system all at the same time, could skew results
RND_WAIT=$(( ( RANDOM % 10 ) + 1 ))
sleep ${RND_WAIT}

jstack=$(find /usr/lib/jvm/ -name jstack | head -n 1)
pid=$(ps -ef | grep "usr/local/tomcat" | grep -v "grep" | grep -v "${SCRIPT_NAME}" | awk -F' ' '{ print $2 }')

${jstack} ${pid} > ${infile}

threads_regex="^\"([a-zA-Z0-9]+(\-|\.| )?)+\""
thread_log_file=""

while read line; do
  if [[ ${line} =~ ${threads_regex} ]]; then 
    output="true" 
    if [[ ${line} =~ ^\".*\-[0-9]+\" ]]; then
      thread_log_file=$(echo ${line} | grep -Eo "\".*\"" | sed -E "s/\"(.*)\-[0-9]+\"/\1/" | sed "s/\s//g")
    elif [[ ${line} =~ ^\".*[0-9]+\" ]]; then
      thread_log_file=$(echo ${line} | grep -Eo "\".*\"" | sed -E "s/\"(.*)[0-9]+\"/\1/" | sed "s/\s//g")
    else
      thread_log_file=$(echo ${line} | grep -Eo "\".*\"" | sed -E "s/\"(.*)\"/\1/" | sed "s/\s//g")
    fi

    if [[ ! ${thread_log_file} =~ ^http.*$ ]]; then
      thread_log_file=$(echo "${thread_log_file}" | sed -E "s/[0-9\-]+//g")
    fi

    thread_log_file="${outfolder}/thread-${thread_log_file}.log"
  # this section for places where we don't want to output threads of a certain type
  elif [[ ${line} =~ ^\" ]]; then 
    output="false" 
  fi

  if [[ "${output}" = "false" ]]; then
    continue
  fi

  if [[ "${output}" = "true" ]] && [[ ${line} =~ ^\" ]]; then 
    echo "$(date +'%Y-%m-%d %H:%M:%S') ${line}" >> ${thread_log_file}
  else 
    echo ${line} >> ${thread_log_file}
  fi

done < ${infile}

rm ${infile}
