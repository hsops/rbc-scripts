#!/bin/bash

# Cron Job
#
# PATH=/usr/lib/sysstat:/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin
# 
# */1 * * * * root /opt/rbc/status/tcpStats.sh

# HSCHG-905 Check to see if this script is currently running, exit if it is
ProcessName=$(basename ${0})
ProcessCheck=$(ps ax | grep ${ProcessName} | grep -v grep | head -n 1 | wc -l)
if [[ ! "${ProcessCheck}" == "1" ]]; then
  echo "Process [${ProcessName}] already running"
  exit 0
fi

# Log TCP Stats to metrics
LOG_FOLDER="/var/log/metrics"

if [[ ! -d "${LOG_FOLDER}" ]]; then
  mkdir -p ${LOG_FOLDER}
fi

TCP_LOG="${LOG_FOLDER}/tcpStats.log"

TIME_WAIT_COUNT=$(netstat -n | grep tcp | grep "TIME_WAIT" | wc -l)
CLOSE_WAIT_COUNT=$(netstat -n | grep tcp | grep "CLOSE_WAIT" | wc -l)
ESTABLISHED_COUNT=$(netstat -n | grep tcp | grep "ESTABLISHED" | wc -l)

DB_TIME_WAIT_COUNT=$(netstat -n | grep tcp | grep "TIME_WAIT" | grep -E "10\.91\.(8|9)\." | wc -l)
DB_CLOSE_WAIT_COUNT=$(netstat -n | grep tcp | grep "CLOSE_WAIT" | grep -E "10\.91\.(8|9)\." | wc -l)
DB_ESTABLISHED_COUNT=$(netstat -n | grep tcp | grep "ESTABLISHED" | grep -E "10\.91\.(8|9)\." | wc -l)

echo "$(date +'%Y-%m-%d %T') TimeWait=${TIME_WAIT_COUNT}, CloseWait=${CLOSE_WAIT_COUNT}, Established=${ESTABLISHED_COUNT}, DBTimeWait=${DB_TIME_WAIT_COUNT}, DBCloseWait=${DB_CLOSE_WAIT_COUNT}, DBEst=${DB_ESTABLISHED_COUNT}" >> ${TCP_LOG}


