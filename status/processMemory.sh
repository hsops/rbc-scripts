#!/bin/bash

# HSCHG-905 Check to see if this script is currently running, exit if it is
ProcessName=$(basename ${0})
ProcessCheck=$(ps ax | grep ${ProcessName} | grep -v grep | head -n 1 | wc -l)
if [[ ! "${ProcessCheck}" == "1" ]]; then
  echo "Process [${ProcessName}] already running"
  exit 0
fi

PROC_MEM_FILE="/var/log/metrics/process-memory.log"


# Disabling this.
exit 0

# PIDs=$(ps ax | grep -v "PID"  | awk -F' ' '{ print $1 }')

#for pid in ${PIDs[@]}; do 
#  pid_name=$(ps ax | grep -E "^\s{0,}${pid}\s{1,}" | awk -F' ' '{ print $5 }') 
#  if [[ ! -z ${pid_name} ]]; then
#    pid_memory=$(echo 0 $(awk '/Pss/ {print "+", $2}' /proc/${pid}/smaps) | bc)
#    if [[ ! -z ${pid_memory} ]] && [[ ${pid_memory} -gt 10000 ]]; then
#      echo "$(date) Process=${pid_name},Memory=${pid_memory}" >> ${PROC_MEM_FILE}
#    fi
#  fi
#done
