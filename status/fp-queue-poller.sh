#!/bin/bash

DO_POLL=$(find "/usr/local/tomcat/webapps/" -name "hsfp.war")

if [[ -z ${DO_POLL} ]]; then

  exit 0

fi

# Log Garbage Collection Times to Splunk
TMP_LOG="/tmp/queue.log"

FPQ_PROCESSES=$(ps ax | grep "curl" | grep "status\.jsp" | grep -v grep | awk -F' ' '{ print $1 }')
# Check for running cron jobs
if [[ ! -z ${FPQ_PROCESSES} ]]; then
  echo "A curl command already in use ... exiting"
  exit 0
fi

curl -s http://127.0.0.1/hsfp/status.jsp | grep "<br/>" > ${TMP_LOG}

RunningCount=$(cat ${TMP_LOG} | grep -Eo "Running:\s+[0-9]+" | awk -F' ' '{ print $2 }')
ThreadCount=$(cat ${TMP_LOG} | grep -Eo "Threads:\s+[0-9]+" | awk -F' ' '{ print $2 }')

if [[ -z ${RunningCount} ]] || [[ -z ${ThreadCount} ]]; then
  RunningCount=0
  ThreadCount=0
fi

LOG_FOLDER="/var/log/metrics"
FPQ_LOG="${LOG_FOLDER}/fp-queue.log"
echo "$(date +'%Y-%m-%d %T') run=${RunningCount}, max=${ThreadCount}" >> ${FPQ_LOG}

curl -s http://127.0.0.1/hsfp/fpdbmapping.jsp | grep "jdbc" > ${LOG_FOLDER}/fpdbmapping.log