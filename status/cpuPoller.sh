#!/bin/bash

# Cron Job
#
# PATH=/usr/lib/sysstat:/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin
# 
# */1 * * * * root /opt/rbc/status/cpuPoller.sh

# HSCHG-905 Check to see if this script is currently running, exit if it is
ProcessName=$(basename ${0})
ProcessCheck=$(ps ax | grep ${ProcessName} | grep -v grep | head -n 1 | wc -l)
if [[ ! "${ProcessCheck}" == "1" ]]; then
  echo "Process [${ProcessName}] already running"
  exit 0
fi

exit 0

# Log cpu usage to metrics folder
LOG_FOLDER="/var/log/metrics"

if [[ ! -d "${LOG_FOLDER}" ]]; then
  mkdir -p ${LOG_FOLDER}
fi

CPU_LOG="${LOG_FOLDER}/cpu-stats.log"

RND_WAIT=$(( ( RANDOM % 20 ) + 10 ))
sleep ${RND_WAIT}

# CPU
mpstat 1 5 | grep -v "CPU" | grep "Average" | awk -F' ' -v date="$(date +'%Y-%m-%d %T')" '{ print date ", usr=" $3 ", nice=" $4 ", sys=" $5 ", iowait=" $6 ", irq=" $7 ", soft=" $8 ", steal=" $9 ", guest=" $10 ", idle=" $12 }' >> ${CPU_LOG}

