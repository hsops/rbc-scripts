#!/bin/bash

uploadfiles()
{
  # This has 2 parameters
  # 1 - Source folder
  # 2 - S3 KeyPrefix
  
  echo "Moving working folder to ${1}"
  pushd ${1}
  
  # Check if there are any files to upload
  if [[ $(ls | grep -E "\.tar\.gz$" | wc -l) -eq 0 ]]; then 
    echo "No files to upload"
    return 0
  fi
  
  for f in $(ls *.tar.gz); do
    datestring=$(echo ${f} | grep -oE "[0-9]{4}(-[0-9]{2}){2}")
	echo ${datestring}
	OLDIFS="${IFS}"
	IFS='-'
	read -a dateparts <<< "${datestring}"
	IFS="${OLDIFS}"
	
	keyyear=${dateparts[0]}
	keymonth=${dateparts[1]}
	keyday=${dateparts[2]}
	
	s3file="${2}/${keyyear}/${keymonth}/${keyday}/${f}"
	echo "s3 key: ${s3file}"
	
	aws s3 cp ${f} ${s3file} --profile backup-logs
	
	# Check if it made it to the other side
	s3return=$(aws s3 ls ${s3file} --profile backup-logs | wc -l)
	if [[ ${s3return} -eq 0 ]]; then
	  echo "File never made it, error 2"
	  return 1
	fi
	
	# Remove local file
	rm ${f}
  done
  
  echo "Moving working folder back"
  popd
}

echo "uploading access-logs"
uploadfiles "/var/log/tomcat/access" "s3://hotschedules/logs/web-app/access-logs"
echo "uploading localhost"
uploadfiles "/var/log/tomcat/localhost" "s3://hotschedules/logs/web-app/localhost"
echo "uploading catalina"
uploadfiles "/var/log/tomcat/catalina" "s3://hotschedules/logs/web-app/catalina"

df -h | grep "/dev/sda"

echo "Completed Archive"
