#!/bin/bash

df -h | grep "/dev/sda"

TODAY="$(date +%Y-%m-%d)"

archivefiles()
{
  # Expects 3 variables
  # 1 - Source location
  # 2 - Target location
  # 3 - Regex of todays files e.g. access_log${TODAY}\.[0-9]{2}\.log
    
  # Go to new location
  echo "Moving to new location ${1}"
  pushd ${1}
  
  # Make sure we don't zip the last file even though we 
  # ignore today's files.  The last file could be from 
  # a few days ago
  LAST_FILE=
  if [[ $(ls | grep -E "\.log$" | wc -l) -gt 0 ]]; then 
    LAST_FILE=$(ls -t *.log | head -1)
  fi
  
  if [[ -z ${LAST_FILE} ]]; then 
    echo "No files to archive"
    return 1
  fi
  
  for f in $(ls *.log); do 
    ftar="${HOSTNAME}.${f}.tar.gz"
    if [[ ! ("${f}" =~ ${3}) ]] && [[ ! -f "${ftar}"  ]] && [[ ${f} != ${LAST_FILE} ]]; then
      echo "File for archive = ${f} -> ${ftar}"
      
	  tar -czf ${ftar} ${f} 
	  if [[ -f ${ftar} ]]; then
	    echo "Removing original file ${f}"
	    rm ${f}
	  else
	    echo "File ${ftar} - not created"
	  fi
    fi
  done
  
  if [[ "${1}" != "${2}" ]]; then
    # Check Target location exists
    if [[ ! -d "${2}" ]]; then
	  echo "Folder doesn\'t exist, creating it"
      mkdir -p ${2}
    fi
	
	# Move the files to target location
	if [[ $(ls | grep -E "\.gz$" | wc -l) -gt 0 ]]; then
      mv *.gz ${2}
	else
	  echo "No files to move"
    fi
  else
    echo "Source and target locations are the same, no need to move files"
  fi
  
  # Go back to former location
  popd
  
  echo "Completed archive"
}

echo "Access Logs"
archivefiles "/usr/local/tomcat/logs/" "/var/log/tomcat/access/" "access_log${TODAY}\.[0-9]{2}\.log"
echo "Catalina Logs"
archivefiles "/var/log/tomcat/catalina/" "/var/log/tomcat/catalina/" "catalina\.${TODAY}\.log"
echo "Localhost Logs"
archivefiles "/var/log/tomcat/localhost/" "/var/log/tomcat/localhost/" "localhost\.${TODAY}\.log"

df -h | grep "/dev/sda"

echo "Completed Archive"
